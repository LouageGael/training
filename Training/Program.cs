﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace Training
{
    public static class Program
    {
        static void Main(string[] args)
        {
          
        }
        /*Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.*/
        public static int[] TwoSum(int[] nums, int target)
        {
            int f = 0;
            int s = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (nums[i] + nums[j] == target)
                    {
                        f = i;
                        s = j;
                        break;
                        //return new int[] {i,j};
                    }
                }
            }
            return new int[] { f, s };
        }
        // convert string to array
        public static string[] StringToArray(string str)
        {
            return str.Split(' ');
        }
        /*Given a string s, return true if it is a palindrome, or false otherwise.*/
        /*Input: s = "A man, a plan, a canal: Panama"
        Output: true
        Explanation: "amanaplanacanalpanama" is a palindrome.*/
        public static bool IsPalindrome(string s)
        {
            string nS = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLetter(s[i]) || char.IsDigit(s[i]))
                {
                    nS += s[i].ToString().ToLower();
                }
            }
            return nS == string.Concat(nS.Reverse());
        }
        /*Given an integer array nums of length n, you want to create an array ans of length 2n where ans[i] == nums[i] and ans[i + n] == nums[i] for 0 <= i < n (0-indexed).*/
        public static int[] GetConcatenation(int[] nums)
        {
            List<int> nL = new List<int>();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < nums.Length; j++)
                {
                    nL.Add(nums[j]);
                }
            }
            return nL.ToArray();
        }
        /*Given a string s, return the string after replacing every uppercase letter with the same lowercase letter.*/
        public static string ToLowerCase(string s)
        {
            return s.ToLower();
        }
        /*Given an integer n, return a string array answer (1-indexed) where:
        answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
        answer[i] == "Fizz" if i is divisible by 3.
        answer[i] == "Buzz" if i is divisible by 5.
        answer[i] == i (as a string) if none of the above conditions are true.*/
        public static IList<string> FizzBuzz(int n)
        {
            List<string> resultL = new List<string>();
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    resultL.Add("FizzBuzz");
                }
                else if (i % 3 == 0)
                {
                    resultL.Add("Fizz");
                }
                else if (i % 5 == 0)
                {
                    resultL.Add("Buzz");
                }
                else
                {
                    resultL.Add(i.ToString());
                }
            }
            return resultL;
        }
        //return sqrt
        public static int MySqrt(int x)
        {
            return (int)Math.Sqrt(x);
        }
        /*Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.*/
        public static bool ContainsDuplicate(int[] nums)
        {

            return nums.ToList().GroupBy(x => x).Where(x => x.Count() > 1).Count() >= 1 ? true : false;
        }
        /*Count the number of occurrences of each character and return it as a list of tuples in order of appearance. For empty output return an empty list.*/
        public static List<Tuple<char, int>> OrderedCount(string input)
        {
            // Implement me!
            List<Tuple<char, int>> nR = new List<Tuple<char, int>>();
            var q = input.GroupBy(x => x);
            foreach (var item in q)
            {
                nR.Add(new Tuple<char, int>(item.Key, item.Count()));
            }
            return nR;
        }
        // check if number is prime (big numbers)
        public static bool IsPrime(int n)
        {
            if (n <= 1) return false;
            if (n == 2) return true;
            if (n % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(n));

            for (int i = 3; i <= boundary; i += 2)
                if (n % i == 0)
                    return false;

            return true;
        }
        /*Complete the solution so that it splits the string into pairs of two characters. If the string contains an odd number of characters then it should replace the
         * missing second character of the final pair with an underscore ('_').*/
        /** 'abc' =>  ['ab', 'c_']
        * 'abcdef' => ['ab', 'cd', 'ef']*/
        public static string[] Solution(string str)
        {
            List<string> result = new List<string>();
            if (str.Length == 1)
            {
                return new string[] { str + "_" };
            }
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (i % 2 == 0)
                {
                    result.Add(str[i] + "" + str[i + 1]);
                }
                if (i == str.Length - 2 && (i - 1) % 2 == 0)
                {
                    result.Add(str[i + 1] + "_");
                }
            }
            return result.ToArray();
        }
        /*"the-stealth-warrior" gets converted to "theStealthWarrior"
         "The-Stealth_Warrior" gets converted to "TheStealthWarrior"*/
        public static string ToCamelCase(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            string result = str[0].ToString();
            for (int i = 1; i < str.Length; i++)
            {
                if (!char.IsLetter(str[i]))
                {
                    result += str[++i].ToString().ToUpper();
                }
                else
                {
                    result += str[i];
                }
            }
            return result;
        }
            /*Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence, which is the number of times you must multiply the digits in num until you reach a single digit.*/
            /*39 --> 3 (because 3*9 = 27, 2*7 = 14, 1*4 = 4 and 4 has only one digit)
    999 --> 4 (because 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, and finally 1*2 = 2)
    4 --> 0 (because 4 is already a one-digit number)*/
            public static int Persistence(long n)
        {
            if (n.ToString().Length <= 1)
            {
                return 0;
            }
            int product = 1;
            int times = 0;
            string num = n.ToString();
            while (num.Length > 1)
            {
                product = 1;
                for (int i = 0; i < num.Length; i++)
                {
                    product *= int.Parse(num[i].ToString());
                }
                num = product.ToString();
                times++;
            }
            return times;
        }
        //Create a function that returns the CSV representation of a two-dimensional numeric array.
        public static string ToCsvText(int[][] array)
        {
            string result = "";
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    if (j < array[i].Length - 1)
                    {
                        result += array[i][j] + ",";
                    }
                    else
                    {
                        result += array[i][j];
                    }
                }
                if (i < array.Length - 1)
                    result += "\n";
            }
            return result;
        }
        // print count of string not between a && m
        public static string PrinterError(String s) => s.Count(x => x < 'a' || x > 'm') + "/" + s.Length;
        /*Implement a function that adds two numbers together and returns their sum in binary. The conversion can be done before, or after the addition.

        The binary number returned should be a string.*/
        public static string SumBinary(int a, int b) => Convert.ToString(a + b, 2);
        /*Given any number of arrays each sorted in ascending order, find the nth smallest number of all their elements.
        All the arguments except the last will be arrays, the last argument is n.*/
        // return arr.SelectMany(e => e).OrderBy(e => e).ToArray()[n - 1];
        public static int NthSmallest(int[][] arr, int n)
        {
            List<int> t = new List<int>();
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    t.Add(arr[i][j]);
                }
            }
            t.Sort();
            return t[n - 1];
        }
        //Time to test your basic knowledge in functions! Return the odds from a list:
        public static List<int> Odds(List<int> values) =>
            values.Where(x => x % 2 != 0).ToList();
        /*Every day you rent the car costs $40. If you rent the car for 7 or more days, you get $50 off your total. Alternatively, if you rent the car for 3 or more days, you get $20 off your total.*/
        public static int RentalCarCost(int d) => d >= 3 && d < 7 ? d * 40 - 20 : d >= 7 ? d * 40 - 50 : d * 40;
        /*Return the number (count) of vowels in the given string
        We will consider a, e, i, o, u as vowels for this Kata (but not y).
        The input string will only consist of lower case letters and/or spaces.*/
        //str.ToLower().Count(c=> "aeiou".IndexOf(c) != -1);
        public static int GetVowelCountShort(string str) => str.Select(x => Regex.IsMatch(x.ToString(), "^[aeiuo]+$")).Where(x => x == true).Count();
        //There is an array with some numbers. All numbers are equal except for one. Try to find it!
        public static int GetUnique(IEnumerable<int> numbers) => numbers.GroupBy(x => x).First(x => x.Count() < 2).Key;
        /*Your message is a string containing space separated words.
        You need to encrypt each word in the message using the following rules:
        The first letter must be converted to its ASCII code.
        The second letter must be switched with the last letter*/
        public static string EncryptThis(string input)
        {
            // Implement me! :)
            string result = "";
            string[] resultSp = input.Split(' ');
            for (int i = 0; i < resultSp.Length; i++)
            {
                for (int j = 0; j < resultSp[i].Length; j++)
                {
                    if (j == 0)
                    {
                        result += Convert.ToInt32(resultSp[i][j]);
                    }
                    else
                    {
                        if (j == resultSp[i].Length - 1)
                        {
                            result += resultSp[i][1];
                        }
                        else if (j == 1)
                        {
                            result += resultSp[i][resultSp[i].Length - 1];
                        }
                        else
                        {
                            result += resultSp[i][j];
                        }

                    }
                }
                if (i != resultSp.Length - 1)
                {
                    result += " ";
                }
            }
            return result;
        }
        /*My grandfather always predicted how old people would get, and right before he passed away he revealed his secret!
        In honor of my grandfather's memory we will write a function using his formula!
        Take a list of ages when each of your great-grandparent died.
        Multiply each number by itself.
        Add them all together.
        Take the square root of the result.
        Divide by two.*/
        /*By using the params keyword, you can specify a method parameter that takes a variable number of arguments. The parameter type must be a single-dimensional array.*/
        public static int PredictAge(params int[] ages) => (int)Math.Sqrt(ages.Sum(x => x * x)) / 2;
        /*Your task is to write a function which returns the sum of following series upto nth term(parameter).
        Series: 1 + 1/4 + 1/7 + 1/10 + 1/13 + 1/16 +...*/
        public static string seriesSum(int n)
        {
             double result = 0;
	         for(int i = 0, deler = 1; i < n ;i++, deler += 3)
	         {
	    	    result += 1.0 / deler;
		  
	          }
               return n != 0 ? result.ToString("F2") : "0.00";  
        }
        public static int[] ArrayDiff(int[] a, int[] b) => a.Where(n => !b.Contains(n)).ToArray();

        // string ends with
        public static bool StringEnd(string str, string ending) => str.EndsWith(ending);
        /*Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.
        Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.*/
        public static string Multiply(string num1, string num2) => (BigInteger.Parse(num1) * BigInteger.Parse(num2)).ToString();
        //Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.
        public static int[] MoveZeroesLinq(int[] arr) => arr.OrderBy(x => x == 0).ToArray();
        //Finish the solution so that it sorts the passed in array of numbers. If the function passes in an empty
        //array or null/nil value then it should return an empty array.
        public static int[] SortNumbersLinq(int[] nums) => nums == null ? new int[] { } : nums.OrderBy(x => x).ToArray();
        //Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
        public static int[] RunningSum(int[] nums)
        {
            List<int> arr = new List<int>();
            for (int i = 0; i < nums.Length; i++)
            {
                if (i == 0)
                {
                    arr.Add(nums[i]);
                }
                else
                {
                    arr.Add(arr[i - 1] + nums[i]);
                }
            }
            return arr.ToArray();
        }
        //Given an array of integers nums which is sorted in ascending order, and an integer target, write a function to search target in nums.
        //If target exists, then return its index. Otherwise, return -1.
        public static int Search(int[] nums, int target) => Array.IndexOf(nums, target);
        //Given two sorted arrays nums1 and nums2 of size m and n respectively,
        //return the median of the two sorted arrays.
        public static double FindMedianSortedArrays(int[] nums1, int[] nums2)
        {
            double mediaan = 0;
            Array.Sort(nums1);
            Array.Sort(nums2);
            List<int> ls = new List<int>();
            for (int i = 0; i < nums1.Length; i++)
            {
                ls.Add(nums1[i]);
            }
            for (int i = 0; i < nums2.Length; i++)
            {
                ls.Add(nums2[i]);
            }
            ls.Sort();
            for (int i = 0; i < ls.Count; i++)
            {
                if (ls.Count % 2 == 0)
                {
                    mediaan = ls[ls.Count / 2 - 1] + ls[ls.Count / 2];
                    mediaan /= 2;
                }
                else
                {
                    mediaan = ls[ls.Count / 2];
                }
            }
            return mediaan;
        }
        /*Find the number with the most digits. 
         * If two numbers in the argument array have the same number of digits, return the first one in the array.*/
        public static int FindLongest(int[] number) => number.OrderByDescending(x => x.ToString().Length).ToList().First();
        //Given an integer as input, can you round it to the next (meaning, "higher") multiple of 5?
        public static int RoundToNext5(int n)
        {
            int result = n;
            while (result % 5 != 0)
            {
                result++;
            }
            return result;
        }
        //Given a sequence of numbers, find the largest pair sum in the sequence.
        public static int LargestPairSum(int[] numbers) => numbers.OrderByDescending(x => x).Take(2).Sum();
        
        /*Write a small function that returns the values of an array that are not odd.
        All values in the array will be integers. Return the good values in the order they are given.*/
        public static int[] NoOdds(int[] values) => values.Where(x => x % 2 == 0).ToArray();
        /*Welcome. In this kata, you are asked to square every digit of a number and concatenate them.
      For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.
      Note: The function accepts an integer and returns an integer*/
        public static int SquareDigits(int n)
        {
            string s = n.ToString();
            string result = "";
            for (int i = 0; i < s.Length; i++)
            {
                result += Math.Pow(int.Parse(s[i].ToString()), 2);
            }
            return int.Parse(result);
        }
        //Given a string s consisting of words and spaces, return the length of the last word in the string.
        /*
         Input: s = "   fly me   to   the moon  "
         Output: 4
         Explanation: The last word is "moon" with length 4.
         */
        public static int LengthOfLastWord(string s)
        {
            string[] splittie = s.Split(' ');
            string result = "";
            for (int i = 0; i < splittie.Length; i++)
            {
                if (!String.IsNullOrEmpty(splittie[i]))
                {
                    result = splittie[i];
                }
            }
            return result.Length;
        }
        //Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.
        public static string CreatePhoneNumberShort(int[] numbers) => $"({numbers[0]}{numbers[1]}{numbers[2]}) {numbers[3]}{numbers[4]}{numbers[5]}-{numbers[6]}{numbers[7]}{numbers[8]}{numbers[9]}";
        //Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
        public static int Reverse(int x)
        {
            string result = new string(x.ToString().Reverse().ToArray());
            if (x < 0)
            {
                result = result.Replace("-", "");
                //check if long negative != int value
                if (long.Parse(result) != (int)long.Parse(result))
                {
                    return 0;
                }
                //returns negative value
                return (int)long.Parse(result) * -1;
            }
            // if value type long != value type int 
            if (long.Parse(result) != (int)long.Parse(result))
            {
                return 0;
            }
            //convert long to int  (int)long.Pars()...
            return (int)long.Parse(result);
        }
        public static int[] TwoSumS(int[] nums, int target)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (nums[i] + nums[j] == target)
                    {
                        return new int[] { i, j };
                    }
                }
            }
            return new int[] { };
        }
        //Check to see if a string has the same amount of 'x's and 'o's. The method must return a boolean and be case insensitive. The string can contain any char.
        //public static bool XO (string input) => input.ToLower().Count(x => x == 'o') ==  input.ToLower().Count(x => x == 'x');
        public static bool XO(string input)
        {
            int o = 0;
            int x = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].ToString().ToLower() == "o")
                {
                    o++;
                }
                else if (input[i].ToString().ToLower() == "x")
                {
                    x++;
                }
            }
            return o == x;
        }
        //Complete the solution so that the function will break up camel casing, using a space between words.
        //"camelCasing"  =>  "camel Casing"
        public static string BreakCamelCaseL(string str) => string.Concat(str.Select(x => char.IsUpper(x) ? " " + x : x.ToString()));
        /*Write a function named repeater() that takes two arguments (a string and a number), and returns a new string where the input string is repeated that many times.*/
        public static string RepeaterS(string s, int n) => string.Concat(Enumerable.Repeat(s, n));
        /*ATM machines allow 4 or 6 digit PIN codes and PIN codes cannot contain anything but exactly 4 digits or exactly 6 digits.
         * If the function is passed a valid PIN string, return true, else return false.*/
        public static bool ValidatePin(string pin) => (pin.Length == 4 || pin.Length == 6) && pin.All(x => char.IsDigit(x));
        //An anagram is the result of rearranging the letters of a word to produce a new word(see wikipedia).
        public static bool IsAnagramShort(string test, string original) => String.Concat(test.ToLower().OrderBy(c => c)) == String.Concat(original.ToLower().OrderBy(c => c));

        /*Simple, given a string of words, return the length of the shortest word(s).
       String will never be empty and you do not need to account for different data types. */
        public static int FindShortLinq(string s) => s.Split(' ').OrderBy(x => x.Length).First().Length;
        //Given the string representations of two integers, return the string representation of the sum of those integers.
        // check for big numbers
        //public static string sumStrings(string a, string b) =>  string.IsNullOrEmpty(a) ? b : String.IsNullOrEmpty(b) ? a : (BigInteger.Parse(a) + BigInteger.Parse(b)).ToString();
        public static string sumStrings(string a, string b) 
        {
            if (string.IsNullOrEmpty(a))
            {
                return b;
            }
            else if (string.IsNullOrEmpty(b))
            {
                return a;
            }
            BigInteger biOne = BigInteger.Parse(a);
            BigInteger biTwo = BigInteger.Parse(b);


            return (biOne + biTwo).ToString();
        }
        /*Digital root is the recursive sum of all the digits in a number.
        Given n, take the sum of the digits of n. If that value has more than one digit, continue reducing in this way until 
        a single-digit number is produced. The input will be a non-negative integer.*/
        public static int DigitalRoots(long n)
        {
            // Your awesome code here!
            string s = n.ToString();
            int res = 0;
            for (int i = 0; i < s.Length; i++)
            {
                res += Convert.ToInt32(s[i].ToString());
            }
            s = res.ToString();

            if (s.Length > 1)
            {
                res = 0;
                for (int i = 0; i < s.Length; i++)
                {

                    res += Convert.ToInt32(s[i].ToString());
                }
            }
            return res;
        }
        //Your task, is to create NxN multiplication table, of size provided in parameter.
        /*1 2 3
          2 4 6
          3 6 9*/
        public static int[,] MultiplicationTable(int size)
        {
            int[,] res = new int[size, size];
            for (int i = 0; i < res.GetLength(0); i++)
            {
                for (int j = 0; j < res.GetLength(1); j++)
                {

                    res[i, j] = (i + 1) * (j + 1);
                }
            }
            return res;
        }
        //In this kata you are required to, given a string, replace every letter with its position in the alphabet.
        //If anything in the text isn't a letter, ignore it and don't return it.
        //string.Join(" ", text.ToLower().Where(c => char.IsLetter(c)) .Select(c => "abcdefghijklmnopqrstuvwxyz".IndexOf(c) + 1).ToArray());
        public static string AlphabetPositionT(string text)
        {
            string alfa = "abcdefghijklmnopqrstuvwxyz";
            text = text.ToLower().Replace(" ", "");
            string res = string.Empty;
            string res2 = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] >= 'a' && text[i] <= 'z')
                {
                    res += (alfa.IndexOf(text[i]) + 1) + " ";

                }
            }
            for (int i = 0; i < res.Length - 1; i++)
            {
                res2 += res[i];
            }
            return res2;
        }
        /*Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.*/
        public static string CreatePhoneNumber(int[] numbers) => int.Parse(string.Concat(numbers)).ToString("(000) 000-0000");
        /*Return an array containing the numbers from 1 to N, where N is the parametered value.
        Replace certain values however if any of the following conditions are met:
        If the value is a multiple of 3: use the value "Fizz" instead
        If the value is a multiple of 5: use the value "Buzz" instead
        If the value is a multiple of 3 & 5: use the value "FizzBuzz" instead
        N will never be less than 1.*/
        public static string[] GetFizzBuzzArray(int n)
        {
            List<string> res = new List<string>();
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    res.Add("FizzBuzz");
                }
                else if (i % 3 == 0)
                {
                    res.Add("Fizz");
                }
                else if (i % 5 == 0)
                {
                    res.Add("Buzz");
                }
                else
                {
                    res.Add(i.ToString());
                }
            }
            return res.ToArray();
        }
        //In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
        public static string HighAndLow(string numbers)
        {
            var q = numbers.Split(' ').ToList().GroupBy(x => Convert.ToInt32(x)).Select(x => x.Key).ToList();
            return q.Max() + " " + q.Min();
        }
        /*Simple, given a string of words, return the length of the shortest word(s).
        String will never be empty and you do not need to account for different data types.*/
        public static int FindShort(string s) => s.Split(' ').OrderBy(x => x.Length).First().Length;
        /*Given an array of integers, find the one that appears an odd number of times.
         There will always be only one integer that appears an odd number of times.*/
        public static int find_itLinq(int[] seq) => seq.GroupBy(x => x).First(x => x.Count() % 2 != 0).Key;
        /*Count the number of Duplicates
        Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. 
        The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.*/
        public static int DuplicateCountTwo(string str) => str.ToLower().GroupBy(x => x).Count(x => x.Count() > 1);
        /*Write a function that takes an array of numbers (integers for the tests) and a target number. 
         * It should find two different items in the array that, when added together, give the target value. 
         * The indices of these items 
         * should then be returned in a tuple / list (depending on your language) like so: (index1, index2).*/
        public static int[] TwoSumming(int[] numbers, int target)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = i + 1; j < numbers.Length; j++)
                {
                    if (numbers[i] + numbers[j] == target)
                    {
                        result.Add(i);
                        result.Add(j);
                    }
                }
            }
            return result.ToArray();
        }
        /* Write a program in C# Sharp to display the number and frequency of number from giving array. (linq).*/
        public static IDictionary<int, int> PrintNumberFrequency(int[] numbers)
        {
            IDictionary<int, int> numbersTimes = new Dictionary<int, int>();
            Array.ForEach(numbers.GroupBy(x => x).ToArray(), x => numbersTimes.Add(x.Key, x.Count()));
            return numbersTimes;
        }
        /*Write Number in Expanded Form
        You will be given a number and you will need to return it as a string in Expanded Form. For example:
        
        Kata.ExpandedForm(12); # Should return "10 + 2"
        Kata.ExpandedForm(42); # Should return "40 + 2"
        Kata.ExpandedForm(70304); # Should return "70000 + 300 */
        public static string ExpandedForm(long num)
        {
            string result = "";
            string number = num.ToString();
            List<string> ml = new List<string>();
            for (int i = 0; i < number.Length; i++)
            {
                if (number[i] != '0')
                {
                    result += number[i];
                    for (int j = i + 1; j < number.Length; j++)
                    {
                        result += "0";
                    }
                    ml.Add(result);
                    result = "";
                }
            }
            string ret = "";
            for (int i = 0; i < ml.Count; i++)
            {
                if (i != ml.Count - 1)
                {
                    ret += ml[i] + " + ";
                }
                else
                {
                    ret += ml[i];
                }

            }
            return ret;
        }
        /*In mathematics, the factorial of a non-negative integer n, denoted by n!, is the product 
         * of all positive integers less than or equal to n. For example: 5! = 5 * 4 * 3 * 2 * 1 = 120. By convention the value of 0! is 1.
        Write a function to calculate factorial for a given input. If input is below 0 or above 12 throw an exception 
        of type ArgumentOutOfRangeException (C#) or IllegalArgumentException (Java) 
        or RangeException (PHP) or throw a RangeError (JavaScript) or ValueError (Python) or return -1 (C).*/
        public static int Factorial(int n)
        {
            if (n >= 0 && n <= 12)
            {
                int facul = 1;
                for (int i = 1; i <= n; i++)
                {
                    facul *= i;
                }
                return facul;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }
        public static string EncryptThisS(string input)
        {
            /*
             [TestCase("A wise old owl lived in an oak", "65 119esi 111dl 111lw 108dvei 105n 97n 111ka")]
             [TestCase("The more he saw the less he spoke", "84eh 109ero 104e 115wa 116eh 108sse 104e 115eokp")]
             [TestCase("The less he spoke the more he heard", "84eh 108sse 104e 115eokp 116eh 109ero 104e 104dare")]
             [TestCase("Why can we not all be like that wise old bird", "87yh 99na 119e 110to 97ll 98e 108eki 116tah 119esi 111dl 98dri")]
             [TestCase("Thank you Piotr for all your help", "84kanh 121uo 80roti 102ro 97ll 121ruo 104ple")]*/
            /*Description:
            Encrypt this!
            You want to create secret messages which can be deciphered by the Decipher this! kata. 
            Here are the conditions:
            Your message is a string containing space separated words.
            You need to encrypt each word in the message using the following rules:
            The first letter must be converted to its ASCII code.
            The second letter must be switched with the last letter
            Keepin' it simple: There are no special characters in the input.*/
            string result = "";
            if (!String.IsNullOrEmpty(input))
            {
                string[] splitted = input.Split(' ');
                for (int i = 0; i < splitted.Length; i++)
                {
                    result += Convert.ToInt32(splitted[i][0]);
                    if (splitted[i].ToString().Length > 1)
                    {
                        result += splitted[i][splitted[i].Length - 1];
                        if (splitted[i].ToString().Length > 2)
                        {
                            for (int j = 2; j < splitted[i].Length - 1; j++)
                            {
                                result += splitted[i][j];
                            }
                            result += splitted[i][1];
                        }
                    }
                    if (splitted.Length - 1 != i)
                    {
                        result += " ";
                    }
                }
            }
            return result;
        }
        public static string Apple(object n)
        {
            string a = Convert.ToString(n);
            if (int.TryParse(a, out int x))
            {
                if (Convert.ToInt32(Math.Pow(x, 2)) > 1000)
                {
                    return "It's hotter than the sun!!";
                }
                else
                {
                    return "Help yourself to a honeycomb Yorkie for the glovebox.";
                }
            }
            return "0";
        }
        /*triangle inequality theorem*/
        /*Implement a function that accepts 3 integer values a, b, c. The function should return true if a triangle can be built with the sides of given length and false in any other case.*/
        public static bool IsTriangle(int a, int b, int c) => a + b > c && a + c > b && b + c > a;
        /*Create a function that returns the sum of the two lowest
         * positive numbers given an array of minimum 4 positive integers.
         * No floats or non-positive integers will be passed.*/
        public static int sumTwoSmallestNumbers(int[] numbers)
        {
            //Code here...
            Array.Sort(numbers);
            return numbers[0] + numbers[1];
        }
        //reverse string
        public static string ReverseWords(string str)
        {
            string res = "";
            string[] sp = str.Split(' ');
            for (int i = sp.Length - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    res += sp[i];
                }
                else
                {
                    res += sp[i] + " ";
                }

            }
            return res;
        }
        public static int[] TwoSums(int[] nums, int target)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < nums.Length - 1; i++)
            {
                if (nums[i] + nums[i + 1] == target)
                {
                    result.Add(i);
                    result.Add(i + 1);
                }
            }
            return result.ToArray();
        }
        /*An anagram is the result of rearranging the letters of a word to produce a new word (see wikipedia).
        Note: anagrams are case insensitive
        Complete the function to return true if the two arguments given are anagrams of each other; return false otherwise.*/
        public static bool IsAnagram2(string test, string original) =>  string.Concat(test.ToLower().OrderBy(x => x).ToList()) == string.Concat(original.ToLower().OrderBy(x => x).ToList());
        //Remove all exclamation marks from sentence but
        //ensure a exclamation mark at the end of string. For a beginner kata,
        //you can assume that the input data is always a non empty string, no need to verify it.
        public static string Remove(string s)
        {
            //    return s.Replace("!","") + "!";
            List<char> ss = s.Where(x => x != '!').ToList();
            string res = "";
            foreach (var item in ss)
            {
                res += item.ToString();
            }
            res += "!";
            return s.Replace("!","");
        }
        /*Return the number (count) of vowels in the given string.
        We will consider a, e, i, o, u as vowels for this Kata (but not y).*/
        public static int GetVowelCount(string str)
        {
            int vowelCount = 0;
            // Your code here
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u')
                {
                    vowelCount++;
                }
            }
            return vowelCount;
        }
        //Each number should be formatted that it is rounded to two decimal places.You don't need to check whether the input is a valid number because only valid numbers are used in the tests.
        public static double TwoDecimalPlaces(double number) => Convert.ToDouble(number.ToString("F2"));

        // square num linq where > 20
        public static IEnumerable<int> SquareArray(int[] arr) => arr.Select(x => x * x).Where(x => x > 20);
        public static int WordsToMarks(string str)
        {
            /*If　a = 1, b = 2, c = 3 ... z = 26
            Then l + o + v + e = 54
            and f + r + i + e + n + d + s + h + i + p = 108
            So friendship is twice stronger than love :-
            The input will always be in lowercase and never be empty.*/
        char[] alfa = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            int counter = 0;
            for (int i = 0; i < alfa.Length; i++)
            {
                for (int j = 0; j < str.Length; j++)
                {
                    if (str[j] == alfa[i])
                    {
                        counter += (i + 1);
                    }
                }
            }
            return counter;
        }
        //Create function fib that returns n'th element of Fibonacci sequence
        public static int Fib(int n)
        {
            if (n == 0) return 1;
            int f = 0;
            int one = 0;
            int two = 1;
            Console.WriteLine(n);
            for (int i = 1; i < n; i++)
            {
                f = one + two;
                one = two;
                two = f;

            }
            return f;
        }
       
        /*Complete the function/method so that it returns the url with anything after the anchor (#) removed.*/
        public static string RemoveUrlAnchor(string url) => url.Split('#')[0];
        /*You are going to be given a word. Your job is to return the middle character of the word. If the word's length is odd, return the middle character.
         * If the word's length is even, return the middle 2 characters.*/
        public static string GetMiddleShort(string s) => s.Length % 2 == 0 ? s[s.Length / 2 - 1].ToString() + s[s.Length / 2].ToString() : s[s.Length / 2].ToString();
        public static string AlphabetPosition(string text)
        {
            /*Welcome.
            In this kata you are required to, given a string, replace every letter with its position in the alphabet.
            If anything in the text isn't a letter, ignore it and don't return it.
            "a" = 1, "b" = 2, etc.
            Example
            Kata.AlphabetPosition("The sunset sets at twelve o' clock.")
            Should return "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11" ( as a string )
            */
            string result = string.Empty;
            char[] alfa = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };
            string textCopy = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (char.IsLetter(text[i]))
                {
                    textCopy += text[i].ToString().ToLower();
                }
            }
            for (int i = 0; i < textCopy.Length; i++)
            {
                for (int j = 0; j < alfa.Length; j++)
                {
                    if (textCopy[i] == alfa[j])
                    {
                        if (i != textCopy.Length - 1)
                        {
                            result += numbers[j] + " ";
                        }
                        else
                        {
                            result += numbers[j];
                        }
                    }
                }
            }
            return result;
        }
        public static int factorial(int n)
        {
            /*Your task is to write function factorial.*/
            int facul = 1;
            for (int i = 1; i <= n; i++)
            {
                facul *= i;
            }
            return facul;
        }
        public static int[,] MultiplicationTables(int size)
        {
            /*Your task, is to create NxN multiplication table, of size provided in parameter.
            for example, when given size is 3:
            1 2 3
            2 4 6
            3 6 9
            for given example, the return value should be: [[1,2,3],[2,4,6],[3,6,9]]*/
            int[,] arrRet = new int[size, size];
            for (int i = 0; i < arrRet.GetLength(1); i++)
            {
                for (int j = 0; j < arrRet.GetLength(0); j++)
                {
                    arrRet[j, i] = (i + 1) * (j + 1);
                }
            }
            return arrRet;
        }
        /*Starting with .NET Framework 3.5, C# supports a
         * Where (in the System.Linq namespace) method which allows a user to filter arrays based 
         * on a predicate. Use the Where method to complete the function given.*/
        public static int[] GetEvenNumbers(int[] numbers) => numbers.Where(x => x % 2 == 0).ToArray();
        /*Find Mean
    Find the mean (average) of a list of numbers in an array.*/
        public static int FindAverage(int[] nums) => (int)nums.Average();
        public static Dictionary<char, int> Count(string str)
        {
            //The main idea is to count all the occurring characters in a string.
            //If you have a string like aba, then the result should be {'a': 2, 'b': 1}.

            // What if the string is empty ? Then the result should be empty object literal, { }.
            Dictionary<char, int> dictionary = new Dictionary<char, int>();
            char[] res = str.ToCharArray();
            var query = from Vs in res
                        group Vs by Vs into x
                        select x;
            foreach (var item in query)
            {
                dictionary.Add(item.Key, item.Count());
            }
            return dictionary;
        }
        public static string[] AddLength(string str)
        {
            /*What if we need the length of the words separated by a space to be added at the end of that same word and have it returned as an array?
            Kata.AddLength("apple ban")    => new string[] {"apple 5", "ban 3"};
            Kata.AddLength("you will win") => new string[] {"you 3", "will 4", "win 3"};
            Your task is to write a function that takes a String and returns an Array/list with the length of each word added to each element .
            Note: String will have at least one element; words will always be separated by a space.*/
            List<string> splitted = new List<string>();
            string[] splittedArr = str.Split(' ');
            for (int i = 0; i < splittedArr.Length; i++)
            {
                splitted.Add(splittedArr[i] + " " + splittedArr[i].Length);
            }
            return splitted.ToArray();
        }
        /*Palindrome strings
        A palindrome is a word, phrase, number, 
        or other sequence of characters which reads the same backward or forward. 
        This includes capital letters, punctuation, and word dividers.
        Implement a function that checks if something is a palindrome. If the input is a number,
        convert it to string first.*/
        public static bool IsPalindrome(object line) => line.ToString() == string.Concat(line.ToString().ToCharArray().Reverse());
        public static string ShowSequence(int n)
        {
            /*We want to generate a function that computes the series 
             * starting from 0 and ending until the given number.*/
            string som = "";
            int thesom = 0;
            if (n > 0)
            {
                for (int i = 0; i <= n; i++)
                {
                    thesom += i;
                    if (i == n)
                    {
                        som += i + " = " + thesom;
                    }
                    else
                    {
                        som += i + "+";
                    }

                }
            }
            else if (n < 0)
            {
                return n + "<0";
            }
            else
            {
                return n + "=0";
            }
            return som;
        }
        /*Given a random non-negative number, you have to return the digits of this number within an array in reverse order.*/
        public static long[] Digitize(long n) => n.ToString().Select(x => long.Parse(x.ToString())).Reverse().ToArray();
        /*get even nums with ienumerable*/
        public static IEnumerable<int> ArrEven(int[] arrNums) => arrNums.Where(x => x % 2 == 0);
        public static int SubstringAppears(string s, string text)
        {
            /*Write a program in C# Sharp to find the number of times a substring appears in a given string*/
            int res = 0;
            string[] split = text.Split(' ');
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i].Contains(s))
                {
                    res++;
                }
            }
            return res;
        }
        public static bool SetAlarm(bool employed, bool vacation)
        {
            /*Write a function named setAlarm which receives two parameters. The first parameter,
             * employed, is true whenever you are employed and the second parameter, vacation is true whenever you are on vacation.

            The function should return true if you are employed and not on vacation (because these are the 
            circumstances under which you need to set an alarm). It should return false otherwise. Examples:*/
            return employed && vacation == false;
        }
        /*Find the uppercase words in a string
         using linq*/
        public static List<string> ReturnUpperCase(string text) => text.Split(' ').ToList().FindAll(x => x == x.ToUpper());
        public static string ReplaceCases(string text)
        {
            /*Write a program in C# Sharp to read a sentence and replace lowercase characters by uppercase and vice-versa.*/
            string result = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                {
                    result += text[i].ToString().ToLower();
                }
                else
                {
                    result += text[i].ToString().ToUpper();
                }
            }
            return result;
        }
        //Write a C# Sharp program to check whether three given integer values are in the range 20..50 inclusive.
        //Return true if 1 or more of them are in the said range otherwise false
        public static bool CheckIntegerBetweenValues(int a, int b, int c) => (a >= 20 && a <= 50) || (b >= 20 && b <= 50) || (c >= 20 && c <= 50);
        public static bool comp(int[] a, int[] b)
        {
            /*Given two arrays a and b write a function comp(a, b) 
             * (orcompSame(a, b)) that checks whether the two arrays have the 
             * "same" elements, with the same multiplicities(the multiplicity of a member is 
             * the number of times it appears). "Same" means, here, that the elements in b are the elements in a squared,
             * regardless of the order.*/
            List<int> element = new List<int>();
            bool isSame = false;
            bool resultC = false;
            if ((a != null && b != null) && (a.Length == b.Length))
            {
                for (int i = 0; i < b.Length; i++)
                {
                    isSame = false;
                    for (int j = 0; j < a.Length; j++)
                    {
                        if (b[i] == Math.Pow(a[j], 2))
                        {
                            isSame = true;
                        }
                    }
                    if (isSame)
                    {
                        element.Add(i);
                    }
                }
                if (element.Count == a.Length)
                {
                    resultC = true;
                }
                else
                {
                    resultC = false;
                }
            }
            return resultC;
        }
        public static int DigitalRoot(int n)
        {
            /*Given n, take the sum of the digits of n. If that value has more than one digit,
             * continue reducing in this way until a single-digit number is produced. The input will 
             * be a non-negative integer.*/
            /*16  -->  1 + 6 = 7
            942  -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
            132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
            493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2*/
            /*my method in javascript
                function digital_root(n) {
                // ...

                let myFunc = num => Number(num);
                var intArr = Array.from(String(n), myFunc);
                let res = 0;
                for (let i = 0; i < intArr.length; i++)
                {
                    res += intArr[i];
                }

                while (res.toString().length > 1)
                {
                    let myFunc2 = num => Number(num);
                    var intArr2 = Array.from(String(res), myFunc2);
                    res = 0;
                    for (let i = 0; i < intArr2.length; i++)
                    {
                        res += intArr2[i];
                    }
                }
                return res;
            }*/
            int res = 0;
            char[] nums = n.ToString().ToCharArray();
            for (int i = 0; i < nums.Length; i++)
            {
                res += int.Parse(nums[i].ToString());
            }
            if (res.ToString().Length > 1)
            {
                char[] sec = res.ToString().ToCharArray();
                res = 0;
                for (int i = 0; i < sec.Length; i++)
                {
                    res += int.Parse(sec[i].ToString());
                }
            }
            return res;
        }
        public static List<string> Number(List<string> lines)
        {
            /*Your team is writing a fancy new text editor and you've been tasked with implementing the line numbering.
            Write a function which takes a list of strings and returns each line prepended by the correct number.
            The numbering starts at 1. The format is n: string. Notice the colon and space in between.*/
            //your code goes here
            List<string> lsLines = new List<string>();
            for (int i = 0; i < lines.Count; i++)
            {
                lsLines.Add((i + 1) + ": " + lines[i]);
            }
            return lsLines;
        }
        public static double[] Tribonacci(double[] signature, int n)
        {
            /*Well met with Fibonacci bigger brother, AKA Tribonacci.
            As the name may already reveal, it works basically like a Fibonacci, but summing the last 3 (instead of 2) numbers of the sequence to generate the next. And, worse part of it, regrettably I won't get to hear non-native Italian speakers trying to pronounce it 😦
            So, if we are to start our Tribonacci sequence with [1, 1, 1] as a starting input (AKA signature), we have this sequence:
            [1, 1 ,1, 3, 5, 9, 17, 31, ...]
            Assert.AreEqual(new double []{1,1,1,3,5,9,17,31,57,105}, variabonacci.Tribonacci(new double []{1,1,1},10));*/
            double one = signature[0];
            double two = signature[1];
            double three = signature[2];
            double tribo = signature[0];
            List<double> arrTribo = new List<double>();
            for (int i = 0; i < n; i++)
            {
                arrTribo.Add(one);
                tribo = one + two + three;
                one = two;
                two = three;
                three = tribo;
            }
            return arrTribo.ToArray();
        }
        public static string BreakCamelCase(string str)
        {
            /*met linq  
             * public static string BreakCamelCase(string str) => string.Concat(str.Select(x => Char.IsUpper(x) ? " " + x : x.ToString()));
             */
            /*Complete the solution so that the function will break up camel casing, using a space between words.
            Example
            "camelCasing"  =>  "camel Casing"
            "identifier"   =>  "identifier"*/
            // complete the function
            string res = "";
            for (int i = 0; i < str.Length; i++)
            {

                if (char.IsUpper(str[i]))
                {
                    res += " " + str[i];
                }
                else
                {
                    res += str[i];
                }
            }
            return res;
        }
        public static string Rot13(string message)
        {
            /*
             * ROT13 is a simple letter substitution cipher that replaces a letter with the letter 13 letters after it in the alphabet. ROT13 is an example of the Caesar cipher.
            Create a function that takes a string and returns the string ciphered with Rot13. If there are numbers or special characters included in the string, they should be returned as they are. 
            Only letters from the latin/english alphabet should be shifted, like in the original Rot13 "implementation".
             */
            char[] alfa = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            char[] alfaUpper = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            string result = string.Empty;
            // your code here
            for (int i = 0; i < message.Length; i++)
            {
                if ((message[i] >= 'a' && message[i] <= 'z') || (message[i] >= 'A' && message[i] <= 'Z'))
                {
                    for (int j = 0; j < alfa.Length; j++)
                    {
                        if (message[i] == alfa[j])
                        {
                            if (j + 13 >= 26)
                            {
                                result += alfa[j + 13 - 26];
                            }
                            else
                            {
                                result += alfa[j + 13];
                            }
                        }
                    }
                    for (int j = 0; j < alfaUpper.Length; j++)
                    {

                        if (message[i] == alfaUpper[j])
                        {
                            if (j + 13 >= 26)
                            {
                                result += alfaUpper[j + 13 - 26];
                            }
                            else
                            {
                                result += alfaUpper[j + 13];
                            }
                        }
                    }
                }
                else if (message[i] >= 0 || message[i] <= 9)
                {
                    result += message[i];
                }
                else
                {
                    result += message[i];
                }
            }
            return result;
        }
        public static int[] MoveZeroes(int[] arr)
        {
            /*Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.*/
            //return arr.OrderBy(x => x == 0).ToArray();  
            // TODO: Program me
            // TODO: Program me
            List<int> a = new List<int>();
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != 0)
                {
                    a.Add(arr[i]);
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                {
                    a.Add(arr[i]);
                }
            }
            return a.ToArray();
        }
        public static String Accum(string s)
        {
            //accum("abcd") -> "A-Bb-Ccc-Dddd"
            //accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
            //accum("cwAt") -> "C-Ww-Aaa-Tttt"
            // your code
            string t = "";
            for (int i = 0; i < s.Length; i++)
            {
                t += s[i].ToString().ToUpper();
                for (int j = 0; j < i; j++)
                {
                    t += s[i].ToString().ToLower();
                }
                if (i != s.Length - 1)
                {
                    t += "-";
                }
            }
            return t;
        }
        public static double Arithmetic(double a, double b, string op)
        {
            // Given two numbers and an arithmetic operator (the name of it, as a string), return the result of the two numbers having that operator used on them.
            //a and b will both be positive integers, and a will always be the first number in the operation, and b always the second.
            //The four operators are "add", "subtract", "divide", "multiply".
            switch (op)
            {
                case "add": return a + b;
                case "subtract": return a - b;
                case "multiply": return a * b;
                case "divide": return a / b;
                default: return 0;
            }
        }
        public static int[] TwoSumLt(int[] numbers, int target)
        {
            //Write a function that takes an array of numbers(integers for the tests) and a target number. It should find two different items in the array that, when added together, give the target value. The indices of these items should then be returned in a tuple / list(depending on your language) like so: (index1, index2).
            //For the purposes of this kata, some tests may have multiple answers; any valid solutions will be accepted.
            //The input will always be valid(numbers will be an array of length 2 or greater, and all of the items will be numbers; target will always be the sum of two different items from that array).
            int[] arrG = new int[2];
            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = i + 1; j < numbers.Length; j++)
                {
                    if (numbers[i] + numbers[j] == target)
                    {
                        arrG[0] = i;
                        arrG[1] = j;
                    }
                }
            }
            return arrG;
        }
        public static int DuplicateCount(string str)
        {
            /*
             * Count the number of Duplicates
            Write a function that will return the count of
            distinct case-insensitive alphabetic characters 
            and numeric digits that occur more than once in the 
            input string. The input string can be assumed to contain
            only alphabets (both uppercase and lowercase) and numeric digits.
                Example
                "abcde" -> 0 # no characters repeats more than once
                "aabbcde" -> 2 # 'a' and 'b'
                "aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
                "indivisibility" -> 1 # 'i' occurs six times
                "Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
                "aA11" -> 2 # 'a' and '1'
                "ABBA" -> 2 # 'A' and 'B' each occur twice
             */
            int counter = 0;
            var myQ = from Vn in str.ToLower()
                      group Vn by Vn into x
                      select x;
            foreach (var item in myQ)
            {
                // Console.WriteLine(item.Key + " " + item.Count());
                if (item.Count() > 1)
                {
                    counter += 1;
                }
            }
            return counter;
        }
        public static void SortArrayNoLib(int[] a)
        {
            int temp = 0;
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = i + 1; j < a.Length; j++)
                {
                    if (a[j] < a[i])
                    {
                        temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
                Console.WriteLine(a[i]);
            }
        }
        // faul methode
        public static int Facul(ref int getal)
        {
            int res = 1;
            for (int i = 1; i <= getal; i++)
            {
                res *= i;
            }
            return res;
        }
        public static IEnumerable<int> GetIntegersFromList(List<object> listOfItems)
        {
            /*In this kata you will create a function that takes
             * a list of non-negative integers and strings and returns a new list with the strings filtered out.*/
            List<int> nums = new List<int>();
            foreach (var item in listOfItems)
            {
                if (item is int)
                {
                    nums.Add((int)item);
                }

            }
            return nums;
        }
        public static bool IsIsogram(string str)
        {
            /*An isogram is a word that has no repeating letters, consecutive or non-consecutive. 
             * Implement a function that determines whether a string that contains only letters is an
             * isogram. Assume the empty string is an isogram. Ignore letter case.*/
            /*javascript
             function isIsogram(str){
                //...
            let isIso = true;
            let t = str.toLowerCase();
             for(let i = 0; i < t.length;i++)
            {
                 for(let j = i+1; j < t.length;j++)
                {
                    if(t[i] == t[j])
                   {
                        isIso = false;              
                  }
                }
             }
             return isIso;
            }*/
            bool isIso = true;
            string t = str.ToLower();
            var q = from vs in t
                    group vs by vs into x
                    select x;
            foreach (var item in q)
            {
                if (item.Count() >= 2)
                {
                    isIso = false;
                }
            }
            return isIso;
        }
        public static int Divisors(int n)
        {
            /*Count the number of divisors of a positive integer n.
            Random tests go up to n = 500000.*/
            // todo
            int num = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    num++;
                }
            }
            return num;
        }
        public static bool IsSquare(int n)
        {
            //Your code goes here!
            /*A square of squares
            You like building blocks. You especially like building blocks that are squares.
            And what you even like more, is to arrange them into a square of square building blocks!
            However, sometimes, you can't arrange them into a square. Instead, you end up with an ordinary rectangle! Those blasted things! If you just had a way to know, whether you're currently working in vain… Wait! That's it!
            You just have to check if your number of building blocks is a perfect square.
            Task
            Given an integral number, determine if it's a square number:
            In mathematics, a square number or perfect square is an integer that is the square of an integer; in other words,
            it is the product of some integer with itself.
            The tests will always use some integral number, so don't worry about that in dynamic typed languages.*/
            return (int)Math.Sqrt(n) * (int)Math.Sqrt(n) == n;
        }
        /*Implement a function that adds two numbers 
         * together and returns their sum in binary.
         * The conversion can be done before, or after the addition.
           The binary number returned should be a string.*/
        public static string AddBinary(int a, int b) => Convert.ToString(a + b, 2).ToString();
        public static int SumDigits(int number)
        {
            /*Write a function named sumDigits which takes a number as input and return
             * s the sum of the absolute value of each of the number's decimal digits.For example: (Input --> Output)
             10 --> 1
            99 --> 18
            -32 --> 5*/
            string nums = Convert.ToString(number * 1);
            int som = 0;
            int[] arrNums = new int[nums.Length];
            if (number < 0)
            {
                for (int i = 1; i < nums.Length; i++)
                {
                    arrNums[i] = int.Parse(nums[i].ToString());
                    som += arrNums[i];
                }
            }
            else
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    arrNums[i] = int.Parse(nums[i].ToString());
                    som += arrNums[i];
                }
            }
            return som;
        }
        public static string Disemvowel(string str)
        {
            /*
             Trolls are attacking your comment section!

            A common way to deal with this situation is to remove all of the vowels from the trolls' comments, neutralizing the threat.
            
            Your task is to write a function that takes a string and return a new string with all vowels removed.
            
            For example, the string "This website is for losers LOL!" would become "Ths wbst s fr lsrs LL!".
            
            Note: for this kata y isn't considered a vowel.*/
            string res = "";
            for (int i = 0; i < str.ToLower().Length; i++)
            {
                if (str[i] != 'a' && str[i] != 'e' &&
                  str[i] != 'i' && str[i] != 'o' &&
                  str[i] != 'u' && str[i] != 'A' & str[i] != 'E' &
                  str[i] != 'I' && str[i] != 'O' &&
                  str[i] != 'U')
                {
                    res += str[i];
                }
            }
            return res;
        }
        public static bool IsAnagram(string test, string original)
        {
            /*An anagram is the result of rearranging the letters of a word to produce a new word (see wikipedia).
            Note: anagrams are case insensitive
            Complete the function to return true if the two arguments
            given are anagrams of each other; return false otherwise.Examples
            "foefet" is an anagram of "toffee"
            "Buckethead" is an anagram of "DeathCubeK"*/
            // your code goes here
            bool isAnagram = false;
            test = test.ToLower();
            original = original.ToLower();
            char[] a = test.ToCharArray();
            char[] b = original.ToCharArray();
            Array.Sort(a);
            Array.Sort(b);
            if (test.Length == original.Length)
            {
                for (int i = 0; i < test.Length; i++)
                {
                    if (a[i] == b[i])
                    {
                        isAnagram = true;
                    }
                    else
                    {
                        isAnagram = false;
                        break;
                    }
                }
            }
            return isAnagram;
        }
        public static bool IsIsogram2(string str)
        {
            /*An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a
             * function that determines whether a string that contains only letters is an isogram.
             * Assume the empty string is an isogram. Ignore letter case.*/
            bool isIso = true;
            // Code on you crazy diamond!
            /*my javascript function*/
            //function isIsogram(str)
            //{
            //    //...
            //    str = str.toLowerCase();
            //    let isIso = true;
            //    for (let i = 0; i < str.length; i++)
            //    {
            //        for (let j = i + 1; j < str.length; j++)
            //        {
            //            if (str[i] == str[j])
            //            {
            //                isIso = false;
            //            }
            //        }
            //    }
            //    return isIso;
            //}
            var q = from Vs in str.ToLower()
                    group Vs by Vs into x
                    select x;
            foreach (var item in q)
            {
                if (item.Count() > 1)
                {
                    isIso = false;
                }
            }
            return isIso;
        }
        public static string Meeting(string s)
        {
            /*John has invited some friends. His list is:

            s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";
            Could you make a program that
            
            makes this string uppercase
            gives it sorted in alphabetical order by last name.
            When the last names are the same, sort them by first name. Last name and first name of a guest come in the result between parentheses separated by a comma.
            
            So the result of function meeting(s) will be:
            
            "(CORWILL, ALFRED)(CORWILL, FRED)(CORWILL, RAPHAEL)(CORWILL, WILFRED)(TORNBULL, BARNEY)(TORNBULL, BETTY)(TORNBULL, BJON)"
            It can happen that in two distinct families with the same family name two people have the same first name too.*/
            // your code
            // your code
            string res = "";
            s = s.ToUpper();
            string[] a = s.Split(';');
            List<string> b = new List<string>();
            for (int i = 0; i < a.Length; i++)
            {
                string[] splittie = a[i].Split(':');
                b.Add("(" + splittie[1] + ", " + splittie[0] + ")");
            }
            foreach (var item in b.OrderBy(x => x))
            {
                res += item;
            }
            return res;
        }
        /*Write a C# Sharp program to concatenate a list of variable parameters.*/
        public static string ConcatStrings(List<string> stringsToCancat) => string.Concat(stringsToCancat);
        public static int CheckExam(string[] arr1, string[] arr2)
        {
            /*The first input array is the key to the correct answers to an
             * exam, like ["a", "a", "b", "d"]. The second one contains a student's submitted answers.

            The two arrays are not empty and are the same length. Return the 
            score for this array of answers, giving +4 for each correct answer, -1 
            for each incorrect answer, and +0 for each blank answer, represented as an 
            empty string (in C the space character is used).

            If the score < 0, return 0.*/
            int points = 0;
            if ((arr1.Length == arr2.Length) && arr1.Length != 0)
            {
                for (int i = 0; i < arr1.Length; i++) // 4 4 -1 0
                {
                    if (arr1[i] == arr2[i])
                    {
                        points += 4;
                    }
                    else
                    {
                        points -= 1;
                    }
                    if (arr2[i] == "")
                    {
                        points += 1;
                    }
                }
            }
            if (points <= 0)
            {
                points = 0;
            }
            return points;
        }
        // is seionor or open
        public static IEnumerable<string> OpenOrSenior(int[][] data)
        {
            //your code here
            List<string> nL = new List<string>();
            for (int i = 0; i < data.Length; i++)
            {

                if (data[i][0] >= 55 && data[i][1] > 7)
                {
                    nL.Add("Senior");
                }
                else
                {
                    nL.Add("Open");
                }
            }
            return nL;
        }
        public static int[] RowWeights(int[] a)
        {
            /*Given an array of positive integers (the weights of the people), return a new array/tuple of two integers,
             * where the first one is the total weight of team 1, and the second one is the total weight of team 2.*/
            //Do Some Magic
            int[] listW = new int[2];
            for (int i = 0; i < a.Length; i++)
            {
                if (i % 2 == 0)
                {
                    listW[0] += a[i];
                }
                else
                {
                    listW[1] += a[i];
                }
            }
            return listW;
        }
        public static void MaxGrade()
        {
            //get the studs with max grade point 
            Students lsStud = new Students();
            var q = lsStud.GtStuRec().OrderByDescending(x => x.GrPoint).First();
            foreach (var item in lsStud.GtStuRec().OrderByDescending(x => x.GrPoint).TakeWhile(x => x.GrPoint == q.GrPoint))
            {
                Console.WriteLine(item.StuName);

            }
        }
        public static string CheckChar(char a)
        {
            /*Write a program in C# Sharp to check whether a
            * character is an alphabet and not and if so, go to check for the case*/
            string res = "";
            if (char.IsLetter(a))
            {
                if (char.IsUpper(a))
                {
                    res = a + " is alfa  upper";
                }
                else
                {
                    res = a + " is alfa but not upper";
                }
            }
            else
            {
                res = a + " is not a letter";
            }
            return res;
        }
        public static string CheckIndexOne(string text)
        {
            /*Write a C# Sharp program to check if a string 'yt' 
             * appears at index 1 in a given string. If it appears return a string without 'yt' 
             * otherwise return the original string*/
            /*Sample Input:
            "Python"
            "ytade"
            "jsues"
            Expected Output:
            Phon
            ytade
            jsues*/
            string res = "";
            if (text[1] == 'y' && text[2] == 't')
            {
                res = text.Substring(0, 1) + "" + text.Substring(3, text.Length - 3);
            }
            else
            {
                res = text;
            }
            return res;
        }
        //ind maximum and minimum element in an array 
        public static int[] MinMax(int[] arr) => new int[] { arr.Min(), arr.Max() };
        /*Given an array of integers, find the one that appears an odd number of times.
        There will always be only one integer that appears an odd number of times.
        Examples
        [7] should return 7, because it occurs 1 time (which is odd).
        [0] should return 0, because it occurs 1 time (which is odd).
        [1,1,2] should return 2, because it occurs 1 time (which is odd).
        [0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
        [1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time (which is odd).*/
        public static int find_it(int[] seq)
        {
            /*in javascript 
             function findOdd(A) {
              //happy coding!
                 let res = 0;
                for(let i = 0; i < A.length;i++)
                {
                    res ^= A[i];
                }
              return res;
            }*/
            var q = from Vi in seq
                    group Vi by Vi into x
                    select x;
            return q.First(x => x.Count() % 2 != 0).Key;
        }
        public static int[] SortNumbers(int[] nums)
        {
            /* simple sort array*/
            if (nums == null)
            {
                return new int[] { };
            }
            else
            {
                Array.Sort(nums);
            }
            return nums;
        }
        public static string ToJadenCase(this string phrase)
        {
            /*Jaden Smith, the son of Will Smith, is the star of films such as The Karate Kid (2010) and After Earth (2013). Jaden is also known for some of his philosophy that he delivers via Twitter. When writing on Twitter, he is known for almost always capitalizing every word. For simplicity, you'll have to capitalize each word,
             * check out how contractions are expected to be in the example below.
            Your task is to convert strings to how they would be written by Jaden Smith. The strings are actual quotes from Jaden Smith, but they are not capitalized in the same way he originally typed them.
            Example:
            Not Jaden-Cased: "How can mirrors be real if our eyes aren't real"
            Jaden-Cased:     "How Can Mirrors Be Real If Our Eyes Are*/
            string result = "";
            string[] splitted = phrase.Split(' ');
            for (int i = 0; i < splitted.Length; i++)
            {
                result += splitted[i][0].ToString().ToUpper();
                for (int j = 1; j < splitted[i].Length; j++)
                {
                    result += splitted[i][j];
                }
                if (i != splitted.Length - 1)
                {
                    result += " ";
                }

            }
            return result;
        }
        private static void GetExtensions()
        {
            /*write a program in C# Sharp to count file extensions and group it using LINQ.*/
            List<string> fileExtensions = new List<string>()
             {
            "ext.frx","txt1.txt","txt2.txt", "txt3.txt","dbf1.dbf",
            "pdf1.pdf","pdf2.pdf","frt1.frt","xml1.xml"
             };
            string[] sp = new string[fileExtensions.Count];
            List<string> l2 = new List<string>();
            for (int i = 0; i < fileExtensions.Count; i++)
            {
                sp = fileExtensions[i].Split('.');
                l2.Add(sp[1]);
            }
            var q = from vs in l2
                    group vs by vs into x
                    select x;
            foreach (var item in q)
            {
                Console.WriteLine(item.Key + ": times " + item.Count());
            }
        }
        public static int GetNearest(int a, int b)
        {
            /*Write a C# Sharp program to check which number 
            nearest to the value 100 among two given integers.
            Return 0 if the two numbers are equal*/
            int tot = 100;
            int res;
            if (a == b)
            {
                res = 0;
            }
            else
            {
                if (tot - a < tot - b)
                {
                    res = a;
                }
                else
                {
                    res = b;
                }
            }
            return res;
        }
        public static bool SmallEnough(int[] a, int limit)
        {
            /*You will be given an array and a limit value.
             * You must check that all values in the array are below or equal 
             * to the limit value. If they are, return true. Else, return false.*/
            int counter = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > limit)
                {
                    counter++;
                }
            }
            if (counter > 0)
            {
                return false;
            }
            return true;
        }
        public static bool StringContainBetween2or4Zs(string text)
        {
            /*Write a C# Sharp program to check if a given string contains between 2 and 4 'z' character*/
            // no uppercases !!!
            int a = 0;
            text.Where(x => x == 'z').ToList().ForEach(x => a++);
            return a >= 2 && a <= 4;
        }
        public static int[] SortArrayDescendingOrder(int[] arrD)
        {
            //Write a program in C# Sharp to sort elements of the array in descending order. no lib
            int temp = 0;
            List<int> arrDescending = new List<int>();
            for (int i = 0; i < arrD.Length; i++)
            {
                for (int j = i + 1; j < arrD.Length; j++)
                {
                    if (arrD[j] > arrD[i])
                    {
                        temp = arrD[i];
                        arrD[i] = arrD[j];
                        arrD[j] = temp;
                    }
                }
                arrDescending.Add(arrD[i]);
            }
            return arrDescending.ToArray();
        }
        //The numbers which produce the remainder 0 after divided by 2
        public static IEnumerable<int> FindRemainderOf0(int[] arrNums) => arrNums.Where(x => x % 2 == 0);
        public static string Priem(int getal)
        {
            // priemgetal
            int teller = 0;
            for (int i = 1; i <= getal; i++)
            {
                if (getal % i == 0)
                {
                    teller++;
                }
            }
            if (teller == 2)
            {
                return getal + " is een priemgetal";
            }
            return getal + " is geen priemgetal";
        }
        // >= 55 && > 7 senior
        /*input =  [[18, 20], [45, 2], [61, 12], [37, 6], [21, 21], [78, 9]]
         output = ["Open", "Open", "Senior", "Open", "Open", "Senior"]*/
        //data.Select(member => member[0] >= 55 && member[1] > 7 ? "Senior" : "Open").ToList();
        public static IEnumerable<string> OpenOrSeniorS(int[][] data)
        {
            //your code here
            List<string> ret = new List<string>();
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i][0] >= 55 && data[i][1] > 7)
                {
                    ret.Add("Senior");
                }
                else
                {
                    ret.Add("Open");
                }
            }
            return ret;
        }
        //Count the number of divisors of a positive integer n.
        public static int Divisors2(int n)
        {
            // todo
            int counter = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    counter++;
                }
            }
            return counter;
        }
        /*In this kata you will create a function that takes a list of non-negative integers and strings and returns a new list with the strings filtered out.*/
        public static IEnumerable<int> GetIntegersFromList2(List<object> listOfItems) => listOfItems.OfType<int>();
        //Write a function named sumDigits which takes a number as input
        //and returns the sum of the absolute value of each of the number's decimal digits.
        public static int SumDigits2(int number)
        {
            string num = (Math.Abs(number)).ToString();
            int sum = 0;
            for (int i = 0; i < num.Length; i++)
            {
                sum += int.Parse(num[i].ToString());
            }
            return sum;
        }
        /*     Implement a function that adds two numbers together and returns their sum in binary.The conversion can be done before, or after the addition.
        The binary number returned should be a string.*/
        public static string AddBinary2(int a, int b) => Convert.ToString(a + b, 2);
        /*Ben has a very simple idea to make some profit: he buys something and sells it again.Of course,
         this wouldn't give him any profit at all if he was simply to buy and sell it at the same price. Instead,
         he's going to buy it for the lowest possible price and sell it at the highest.*/
        public static int[] minMax(int[] lst) => new int[] { lst.Min(), lst.Max() };

        /*Write a function that takes a single string (word) as argument. 
         * The function must return an ordered list containing  
         * the indexes of all capital letters in the string.*/
        public static int[] Capitals(string word)
        {
            //Write your code here
            List<int> c = new List<int>();
            for (int i = 0; i < word.Length; i++)
            {
                if (char.IsUpper(word[i]))
                {
                    c.Add(i);
                }
            }
            return c.ToArray();
        }
        /*A square of squares
        You like building blocks. You especially like building blocks that are squares. And what you even like more, is to arrange them into a square of square building blocks!
        However, sometimes, you can't arrange them into a square. Instead, you end up with an ordinary rectangle! Those blasted things! If you just had a way to know, 
        whether you're currently working in vain… Wait! That's it! You just have to check if your number of building blocks is a perfect square.*/
        public static bool IsSquares(int n) => (int)Math.Sqrt(n) * (int)Math.Sqrt(n) == n ? true : false;

        /*Your task is to write a function that takes a string and return a new string with all vowels removed.

        For example, the string "This website is for losers LOL!" would become "Ths wbst s fr lsrs LL!".*/
        public static string Disemvowel2(string str)
        {
            string text = "";
            for (int i = 0; i < str.ToLower().Length; i++)
            {
                if (str[i].ToString().ToLower() != "a" && str[i].ToString().ToLower() != "e" && str[i].ToString().ToLower() != "i" && str[i].ToString().ToLower() != "o" && str[i].ToString().ToLower() != "u")
                {
                    text += str[i];
                }

            }
            return text;
        }
        /*Write a function named repeater() that takes two arguments (a string and a number), and returns a new string where the input string is repeated that many times.*/
        public static string Repeater(string s, int n)
        {
            //Your code goes here.
            string res = "";
            for (int i = 0; i < n; i++)
            {
                res += s;
            }
            return res;
        }
        // Replace all vowel to exclamation mark in the sentence.aeiouAEIOU is vowel.
        public static string Replace(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].ToString().ToLower() == "a" || s[i].ToString().ToLower() == "e" ||
                     s[i].ToString().ToLower() == "i" || s[i].ToString().ToLower() == "o" || s[i].ToString().ToLower() == "u")
                {
                    s = s.Replace(s[i], '!');
                }
            }
            return s;
        }
        /*Complete the function/method so that it returns the url with anything after the anchor (#) removed.*/
        public static string RemoveUrlAnchorL(string url) => url.Split('#')[0];
        /*Write a C# Sharp program to convert the last 3 characters of a
         * given string in upper case. If the length of the string has less 
         * than 3 then uppercase all the characters.*/
        public static string Last3ToUpper(string text) => text.Length >= 3 ? string.Concat(text.Substring(0, text.Length - 3)) +
                                                string.Concat(text.Substring(text.Length - 3, 3).ToUpper()) :
                                                text.ToUpper();
        public static string PeopleWithAgeDrink(int old) => old < 14 ? "drink toddy" : old < 18 ? "drink coke" : old < 21 ? "drink beer" : old >= 21 ? "drink whisky" : "";
        /*Given a string str, reverse it omitting all non-alphabetic characters.
        Example
        For str = "krishan", the output should be "nahsirk".
        For str = "ultr53o?n", the output should be "nortlu".
        Input/Output
        [input] string str
        A string consists of lowercase latin letters, digits and symbols.
        [output] a string*/
        public static string ReverseLetter(string str) => string.Concat(str.Where(char.IsLetter).Reverse());

        // order by lastname
        public static List<Person> SortAlfaLastName(List<Person> ls) => ls.OrderBy(x => x.LastName).ToList();
        // Write a C# Sharp program to check if two given integers have the same last digit
        public static bool SameLastdigit(int a, int b) => a.ToString().Substring(a.ToString().Length - 1, 1) == b.ToString().Substring(b.ToString().Length - 1, 1);
        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }

            public override string ToString() => FirstName + " " + LastName;
        }
        // Write a program in C# Sharp to insert New value in the array (sorted list )
        public static int[] AddIntSortedArray(int[] array, int addy)
        {
            List<int> lisAddy = array.ToList();
            lisAddy.Add(addy);
            lisAddy.Sort();
            return lisAddy.ToArray();
        }
        //Your task is simply to count the total number of lowercase letters in a string.
        public static int LowercaseCountCheck(string s)
        {
            // your code goes here ...
            int counter = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLower(s[i])) counter++;
            }
            return counter;
        }
        //You are given an array with positive numbers and a
        //non-negative number N.You should find the N-th power of the
        //element in the array with the index N.If N is outside of the array,
        //then return -1. Don't forget that the first element has the index 0.
        public static double Index(int[] array, int n) => array.Length > n ? Math.Pow(array[n], n) : -1;
        public static int[] NoOddsS(int[] values) => values.Where(x => x % 2 == 0).ToArray();
        /*You are required to create a simple calculator that returns the result of addition, subtraction, multiplication or division of two numbers.
        Your function will accept three arguments:
        The first and second argument should be numbers.
        The third argument should represent a sign indicating the operation to perform on these two numbers.    
        If the sign is not a valid sign, throw an ArgumentException.*/
        public static double Calculator(double a, double b, char op)
        {
            double result = 0;
            switch (op)
            {
                case '+':
                    result = a + b;
                    break;
                case '-':
                    result = a - b;
                    break;
                case '/':
                    result = a / b;
                    break;
                case '*':
                    result = a * b;
                    break;
                default:
                    throw new ArgumentException();
            }
            return result;
        }
        /*In a small town the population is p0 = 1000 at the beginning of a year. The population regularly increases by 2 percent per year and moreover 50 new inhabitants per year come to live in the town. 
        * How many years does the town need to see its population greater or equal to p = 1200 inhabitants?*/
        public static int NbYear(int p0, double percent, int aug, int p)
        {
            // your code
            // 2 per per y
            // +-50 in pj
            double yearsNeeded = p0 + p0 * percent / 100 + aug;
            int counter = 1;
            while (yearsNeeded < p)
            {
                yearsNeeded = Convert.ToInt32(yearsNeeded) + Convert.ToInt32(yearsNeeded) * percent / 100 + Convert.ToInt32(aug);
                counter++;
            }
            return counter;
        }
        //Return a new array consisting of elements which are multiple of their own index in input array (length > 1).
        //xs.Where((e,i)=> i > 0 && e%i == 0).ToList();

        public static List<int> MultipleOfIndex(List<int> xs)
        {
            List<int> rs = new List<int>();
            for (int i = 1; i <= xs.Count - 1; i++)
            {
                if (xs[i] % i == 0)
                {
                    rs.Add(xs[i]);
                }
            }
            return rs;
        }
        //Remove n exclamation marks in the sentence from left to right. n is positive integer.
        public static string Remove(string s, int n)
        {
            string res = "";
            int c = 0;
            for (int j = 0; j < s.Length; j++)
            {
                if (c != n)
                {
                    if (s[j] == '!')
                    {
                        c++;
                    }
                    else
                    {
                        res += s[j];
                    }
                }
                else
                {
                    res += s[j];
                }

            }
            return res;
        }
        //Your task is to make a function that can take any non-negative integer as an argument and return it with its digits in descending order.
        //Essentially, rearrange the digits to create the highest possible number.
        public static int DescendingOrder(int num) => int.Parse(string.Concat(num.ToString().OrderByDescending(x => x).ToArray()));
        /*Deoxyribonucleic acid (DNA) is a chemical found in the nucleus of cells and carries the "instructions" for the development and functioning of living organisms.
        If you want to know more: http://en.wikipedia.org/wiki/DNA
        In DNA strings, symbols "A" and "T" are complements of each other, as "C" and "G". Your function receives one side of the DNA (string, except for Haskell); you need to return the other complementary side. DNA strand is never empty or there is no DNA at all (again, except for Haskell).*/
        public static string MakeComplement(string dna)
        {
            //Your code
            string res = "";
            for (int i = 0; i < dna.Length; i++)
            {
                switch (dna[i])
                {
                    case 'A':
                        res += "T";
                        break;
                    case 'T':
                        res += "A";
                        break;
                    case 'G':
                        res += "C";
                        break;
                    case 'C':
                        res += "G";
                        break;
                }
            }
            return res;
        }
        /*Given two integers a and b, which can be positive or negative,
         * find the sum of all the integers between and including them and return it. 
         * If the two numbers are equal return a or b.*/
        public static int GetSum(int a, int b)
        {
            int c = 0;
            if (a == b) return b;
            if (a < b)
            {
                for (int i = a; i <= b; i++)
                {
                    c += i;
                }
            }
            if (b < a)
            {
                for (int i = b; i <= a; i++)
                {
                    c += i;
                }
            }
            return c;
        }
        /*Given an array of integers, remove the smallest value.
         * Do not mutate the original array/list. If there are multiple
         * elements with the same value, remove the one with a lower index. If you get an 
         * empty array/list, return an empty array/list.
         Don't change the order of the elements that are left*/
        public static List<int> RemoveSmallest(List<int> numbers)
        {
            // Good Luck!
            if (numbers == null || numbers.Count == 0) return new List<int>();
            numbers.Remove(numbers.Min());
            return numbers;
        }
        // perfect square
        public static bool IsSquareMath(int n) => (int)Math.Sqrt(n) * (int)Math.Sqrt(n) == n ? true : false;
        /*There is a bus moving in the city, and it takes and drop some people in each bus stop.
        You are provided with a list (or array) of integer pairs. Elements of each pair represent number of people get into bus (The first item) and number of people get off the bus (The second item) in a bus stop.
        Your task is to return number of people who are still in the bus after the last bus station (after the last array). Even though it is the last bus stop, the bus is not empty and some people are still in the bus, and they are probably sleeping there :D
        Take a look on the test cases.   
        Please keep in mind that the test cases ensure that the number of people in the bus is always >= 0. So the return integer can't be negative.
        The second value in the first integer array is 0, since the bus is empty in the first bus stop.*/
        public static int Number(List<int[]> peopleListInOut)
        {
            //	public static int Number(List<int[]> peopleListInOut)  =>  peopleListInOut.Select(x => x[0] - x[1]).Sum();
            int inbus = 0;
            int outbus = 0;
            var q = peopleListInOut.Select(x => x).ToList();
            foreach (var item in q)
            {
                inbus += item[0];
                outbus += item[1];
            }
            return inbus - outbus;
        }
        //Find the sum of all multiples of n below m
        public static int SumMul(int n, int m)
        {
            // your idea here
            int g = 0;
            for (int i = 1; i <= m; i++)
            {
                if (i % n == 0)
                {
                    g += i;
                }
            }
            return g;
        }
        //Finish the solution so that it sorts the passed in array of numbers.
        //If the function passes in an empty array or null/nil value then it should return an empty array.
        public static int[] SortNumbersTwo(int[] nums)
        {
            if (nums != null)
            {
                Array.Sort(nums);
                return nums;
            }
            return new int[] { };
        }
        /*You get any card as an argument. Your task is to return the suit of this card (in lowercase).
        Our deck (is preloaded):*/
        public static string[] Deck =
        {
            "2♣", "3♣", "4♣", "5♣", "6♣", "7♣", "8♣", "9♣", "10♣", "J♣", "Q♣", "K♣", "A♣",
            "2♦", "3♦", "4♦", "5♦", "6♦", "7♦", "8♦", "9♦", "10♦", "J♦", "Q♦", "K♦", "A♦",
            "2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥", "10♥", "J♥", "Q♥", "K♥", "A♥",
            "2♠", "3♠", "4♠", "5♠", "6♠", "7♠", "8♠", "9♠", "10♠", "J♠", "Q♠", "K♠", "A♠"
        };
        public static string DefineSuit(string card)
        {
            return card.Contains("♣") ? "clubs" : card.Contains("♦") ? "diamonds" :
                     card.Contains("♥") ? "hearts" : card.Contains("♠") ? "spades" : "";
        }
        //Your task is to find the nearest square number, nearest_sq(n), of a positive integer n.
        public static int NearestSq(int n)
        {
            int sq = n;
            int sqMin = n;
            Console.WriteLine(n);
            if (n == 1)
            {
                return 1;
            }
            while (Math.Sqrt(sq) % 1 != 0 && Math.Sqrt(sqMin) % 1 != 0)
            {
                sq++;
                sqMin--;
                if (Math.Sqrt(sq) % 1 == 0)
                {
                    return sq;
                }
                if (Math.Sqrt(sqMin) % 1 == 0)
                {
                    return sqMin;
                }
            }
            return 1;
        }
        public static void If(bool condition, Action func1, Action func2)
        {
            if (condition)
            {
                func1();
            }
            else
            {
                func2();
            }
        }
        /*You might know some pretty large perfect squares. But what about the NEXT one?
         Complete the findNextSquare method that finds the next integral perfect square after the one passed as a parameter. Recall that an integral perfect square is an integer n such that sqrt(n) is also an integer.
        If the parameter is itself not a perfect square then -1 should be returned. You may assume the parameter is non-negative.*/
        public static long FindNextSquare(long num) => Math.Sqrt(num) % 1 == 0 ? (long)Math.Pow(Math.Sqrt(num) + 1, 2) : -1;
        /*A western man is trying to find gold in a river. To do that, he passes a bucket through the river's soil and then checks if it contains any gold. However, he could be more productive if he wrote an algorithm to do the job for him.
        So, you need to check if there is gold in the bucket, and if so, return True/true. If not, return False/false.*/
        public static bool CheckTheBucket(string[] bucket) => bucket.Where(x => x.Contains("gold")).ToArray().Length > 0;
        /*Create a function called _if which takes 3 arguments: a boolean value bool and 2 functions (which do not take any parameters): func1 and func2
         * When bool is truth-ish, func1 should be called, otherwise call the func2.*/
        /*Complete the function power_of_two/powerOfTwo (or equivalent, depending on your language) that determines if a given non-negative integer is a power of two. From the corresponding Wikipedia entry:
        a power of two is a number of the form 2n where n is an integer, i.e. the result of exponentiation with number two as the base and integer n as the exponent.
        You may assume the input is always valid.*/
        public static bool PowerOfTwo(int n)
        {
            if (n == 1) return true;
            bool isPow = false;
            if (n == 0) return false;
            while (n > 0 && n % 2 == 0)
            {
                n = n / 2;

                if (n == 1)
                {
                    isPow = true;
                }
                else
                {
                    isPow = false;
                }
            }
            return isPow;
        }
        /*Your team is writing a fancy new text editor and you've been tasked with implementing the line numbering.
         *Write a function which takes a list of strings and returns each line prepended by the correct number.
         *The numbering starts at 1. The format is n: string. Notice the colon and space in between.*/
        public static List<string> NumberWithStart(List<string> lines)
        {
            //your code goes here
            List<string> numbering = new List<string>();
            for (int i = 0; i < lines.Count; i++)
            {
                {
                    numbering.Add((i + 1) + ": " + lines[i]);
                }
            }
            return numbering;
        }
        /*The main idea is to count all the occurring characters in a string. If you have a string like aba, then the result should be {'a': 2, 'b': 1}.*/
        public static Dictionary<char, int> CountChars(string str)
        {
            Dictionary<char, int> numberNames = new Dictionary<char, int>();

            var q = str.GroupBy(x => x);
            foreach (var item in q)
            {
                numberNames.Add(item.Key, item.Count());
            }
            return numberNames;
            // return str.GroupBy(c => c).ToDictionary(g => g.Key, g => g.Count());
        }
        /*The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"]. The second one contains a student's submitted answers.
        The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for each incorrect answer, and +0 for each blank answer, represented as an empty string (in C the space character is used).
        If the score < 0, return 0.*/
        public static int CheckExamPoints(string[] arr1, string[] arr2)
        {
            int punten = 0;
            for (int i = 0; i < arr2.Length; i++)
            {
                if (arr2[i] == arr1[i])
                {
                    punten += 4;
                }
                else if (arr2[i] != "")
                {
                    punten--;
                }
            }
            return punten < 0 ? 0 : punten;
        }
        public static class Kata
        {
            public static int CheckExam(string[] arr1, string[] arr2)
            {
                int punten = 0;
                for (int i = 0; i < arr2.Length; i++)
                {
                    if (arr2[i] == arr1[i])
                    {
                        punten += 4;
                    }
                    else if (arr2[i] != "")
                    {
                        punten--;
                    }
                }
                return punten < 0 ? 0 : punten;
            }
        }
        /*In a small town the population is p0 = 1000 at the beginning of a year.
         * The population regularly increases by 2 percent per year and moreover 50 new
         * inhabitants per year come to live in the town. How many years does the town need 
         * to see its population greater or equal to p = 1200 inhabitants?*/
        public static int CYear(int p0, double percent, int aug, int p)
        {
            // your code
            double firstYear = p0 + p0 * (percent / 100) + aug;
            int year = 1;
            while (firstYear < p)
            {
                firstYear = Math.Floor(firstYear + firstYear * (percent / 100) + aug);
                year++;
            }
            return year;
        }
        /*Make a program that filters a list of strings and returns a list with only your friends name in it.
        If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise, you can be sure he's not...*/
        public static IEnumerable<string> FriendOrFoe(string[] names) => names.Where(x => x.Length == 4);
        //Bob needs a fast way to calculate the volume of a cuboid with three values:
        //the length, width and height of the cuboid. Write a function to help Bob with this calculation.
        public static double GetVolumeOfCubiod(double length, double width, double height) => length * width * height;
        public static string Derive(double coefficient, double exponent) => (coefficient * exponent).ToString() + "x^" + (exponent - 1);
       // In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
        public static string HighAndLowLinq(string numbers) => (numbers.Split(' ').Select(x => Convert.ToInt32(x)).Max() + " " + numbers.Split(' ').Select(x => Convert.ToInt32(x)).Min());
        //Starting with .NET Framework 3.5, C# supports a Where (in the System.Linq namespace) method which allows a user to filter arrays based on a predicate. Use the Where method to complete the function given.
        public static int[] GetEvenNumbersShort(int[] numbers) => numbers.Where(x => x % 2 == 0).ToArray();
        /*Create a class Ball. Ball objects should accept one argument for "ball type" when instantiated.
        If no arguments are given, ball objects should instantiate with a "ball type" of "regular."*/
        public class Ball
        {
            public string ballType { get; set; } = "Super";

            public Ball(string ballType)
            {
                // your code goes here
                this.ballType = ballType;
            }
            public Ball()
            {
                // your code goes here
                this.ballType = "regular";
            }
        }
        //get word with higest score
        public static string High(string s)
        {
            List<int> scoreArr = new List<int>();
            string alfa = "abcdefghijklmnopqrstuvwxyz";
            s = s.ToLower();
            string[] sp = s.Split(' ');
            int score = 0;
            for (int i = 0; i < sp.Length; i++)
            {
                for (int j = 0; j < sp[i].Length; j++)
                {

                    for (int z = 0; z < alfa.Length; z++)
                    {
                        if (sp[i][j] == alfa[z])
                        {
                            score += alfa.IndexOf(alfa[z]);
                        }
                    }

                }
                Console.WriteLine(i + 1 + " " + sp[i] + " : " + score);
                scoreArr.Add(score);
                score = 0;
            }

            return sp[scoreArr.IndexOf(scoreArr.Max())];
        }
        //An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines
        //whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.

        // return str.ToUpper().GroupBy(s => s).All(s => s.Count() <= 1);
        public static bool IsIsograms(string str)
        {
            // Code on you crazy diamond!
            str = str.ToLower();
            var q = str.GroupBy(x => x);
            foreach (var item in q)
            {
                if (item.Count() > 1)
                {
                    return false;
                }
            }
            return true;
        }
        /*Not Jaden-Cased: "How can mirrors be real if our eyes aren't real"
        Jaden-Cased:     "How Can Mirrors Be Real If Our Eyes Aren't Real"*/
        public static string ToJadenCaseses(this string phrase)
        {
            string result = "";
            string[] sp = phrase.Split(' ');
            for (int i = 0; i < sp.Length; i++)
            {
                result += sp[i][0].ToString().ToUpper();
                for (int j = 1; j < sp[i].Length; j++)
                {
                    result += sp[i][j].ToString();
                }
                if (i != sp.Length - 1)
                {
                    result += " ";
                }
            }
            return result;
        }
        /*Given a mixed array of number and string representations of integers, add up the string integers and subtract this from the total of the non-string integers.*/
        public static int DivCon(Object[] objArray)
        {
            int typeString = 0;
            int nonString = 0;
            foreach (var item in objArray)
            {
                if (item.GetType() == typeof(string))
                {
                    typeString += Convert.ToInt32(item);
                }
                else if (item.GetType() == typeof(int))
                {
                    nonString += Convert.ToInt32(item);
                }
            }
            return nonString - typeString;
        }
        public static bool IsAscOrder(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i] > arr[i + 1])
                {
                    return false;
                }
            }
            return true;
        }
        public class Students
        {
            public string StuName { get; set; }
            public int GrPoint { get; set; }
            public int StuId { get; set; }

            public List<Students> GtStuRec()
            {
                List<Students> stulist = new List<Students>();
                stulist.Add(new Students { StuId = 1, StuName = " Joseph ", GrPoint = 800 });
                stulist.Add(new Students { StuId = 2, StuName = "Alex", GrPoint = 458 });
                stulist.Add(new Students { StuId = 3, StuName = "Harris", GrPoint = 900 });
                stulist.Add(new Students { StuId = 4, StuName = "Taylor", GrPoint = 900 });
                stulist.Add(new Students { StuId = 5, StuName = "Smith", GrPoint = 458 });
                stulist.Add(new Students { StuId = 6, StuName = "Natasa", GrPoint = 700 });
                stulist.Add(new Students { StuId = 7, StuName = "David", GrPoint = 750 });
                stulist.Add(new Students { StuId = 8, StuName = "Harry", GrPoint = 700 });
                stulist.Add(new Students { StuId = 9, StuName = "Nicolash", GrPoint = 597 });
                stulist.Add(new Students { StuId = 10, StuName = "Jenny", GrPoint = 750 });
                return stulist;
            }
        }
    }
}
